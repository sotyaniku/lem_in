/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_one_ant.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksarnyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/23 14:49:30 by ksarnyts          #+#    #+#             */
/*   Updated: 2017/03/05 13:53:23 by ksarnyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static t_list	*lst_rev(t_list *path)
{
	t_list *tmp;
	t_list *new;

	new = NULL;
	tmp = path;
	while (tmp)
	{
		ft_lstadd(&new, ft_lstnew(tmp->content, tmp->content_size));
		tmp = tmp->next;
	}
	return (new);
}

static int		check_duplicates(t_list *path1, t_list *path2)
{
	t_list *tmp1;
	t_list *tmp2;

	tmp1 = path1->next;
	tmp2 = path2->next;
	while (tmp1 && tmp2)
	{
		if (!ft_strcmp((tmp1->content), (tmp2->content)))
			return (0);
		tmp1 = tmp1->next;
		tmp2 = tmp2->next;
	}
	return (1);
}

static void		check_end(int start, int end, t_list *path, t_path *path_min)
{
	if (start == end)
	{
		if (g_steps < g_min)
		{
			ft_lstdel(&(path_min->p1), &ft_lst_cont_clean);
			path_min->p1 = ft_lstsave(path);
			g_min = g_steps;
		}
		else if (g_steps - 1 <= g_min && path_min->p1\
				&& check_duplicates(lst_rev(path_min->p1), path))
		{
			ft_lstdel(&(path_min->p2), &ft_lst_cont_clean);
			path_min->p2 = ft_lstsave(path);
		}
	}
}

static void		check_path_min(t_path **path_min)
{
	if (*path_min == NULL)
	{
		*path_min = malloc(sizeof(t_path));
		(*path_min)->p1 = NULL;
		(*path_min)->p2 = NULL;
	}
}

t_path			*move_ants(t_room *rooms, int size, int start, int end)
{
	int				index;
	int				j;
	static t_list	*path;
	static t_path	*path_min;

	check_path_min(&path_min);
	check_end(start, end, path, path_min);
	j = 0;
	while (rooms[start].linked_rooms[j] != NULL)
	{
		index = find_index(rooms, size, rooms[start].linked_rooms[j]);
		if (!move_ant(rooms, size, rooms[start].name, rooms[index].name))
			j++;
		else
		{
			ft_lstadd(&path, ft_lstnew(rooms[index].name,\
						ft_strlen(rooms[index].name) + 1));
			move_ants(rooms, size, index, end);
			ft_lstrem(&path);
			move_back(rooms, size, rooms[start].name, rooms[index].name);
			j++;
		}
	}
	return (path_min);
}
