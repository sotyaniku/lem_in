/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksarnyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/16 10:46:36 by ksarnyts          #+#    #+#             */
/*   Updated: 2017/03/05 15:42:33 by ksarnyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_IN_H
# define LEM_IN_H

# include "libft/libft.h"
# include "libft/printf/ft_printf.h"
# include <mlx.h>

# define MAX(x,y) x > y ? x : y;
# define MIN(x,y) x < y ? x : y;

int				g_steps;
int				g_min;

typedef struct	s_room
{
	char	*name;
	char	**linked_rooms;
	int		current_ant_num;
	int		x;
	int		y;
	int		start;
	int		end;
	int		visited;
	int		current_ant;
	int		color;
}				t_room;

typedef struct	s_path
{
	t_list *p1;
	t_list *p2;
	t_list *rev1;
	t_list *rev2;
	t_list *tmp1;
	t_list *tmp2;
}				t_path;

typedef struct	s_param
{
	void	*mlx;
	void	*win;
	int		width;
	int		height;
	int		top;
	t_room	*rms;
	int		room_num;
	char	**map;
}				t_param;

typedef struct	s_data
{
	int min_x;
	int max_x;
	int min_y;
	int max_y;
	int width;
	int height;
}				t_data;

typedef struct	s_point
{
	int x;
	int y;
	int color;
}				t_point;

char			*check_valid(int fd);
int				check_line(char *line);
int				count_rooms(char **map);
int				create_rooms(char **map, t_room *rooms, int start, int end);
int				create_links(char **map, t_room *rooms, int size);
int				find_way(t_room *rooms, int size, t_path **path);
void			ft_lstrem(t_list **alst);
t_list			*ft_lstsave(t_list *lst);
int				start_room(t_room *rooms, int size);
int				end_room(t_room *rooms, int size);
t_path			*move_ants(t_room *rooms, int size, int start, int end);
int				move_ant(t_room *rooms, int size, char *from, char *to);
int				move_ant_count(t_room *rooms, int size, char *from, char *to);
int				find_index(t_room *rooms, int size, char *from);
void			free_all(t_room *rooms, int room_num, t_path *path,\
				char *map_content);
int				free_array(char **array);
void			ft_lst_cont_clean(void *content, size_t size);
void			draw_rooms(t_param *a);
void			print_lines(t_point p1, t_point p2, t_param *a);
void			draw_path(t_room *rooms, int room_num, char **map);
void			draw_rooms(t_param *a);
void			draw_lines(t_param *a);
void			draw_ants(t_param *a, t_room room, int x, int y);
void			modify_output(t_param *a);
int				move_back(t_room *rooms, int size, char *to, char *from);
void			move_all_to_end(t_room *rooms, int size);

#endif
