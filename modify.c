/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   modify.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksarnyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/03 14:54:36 by ksarnyts          #+#    #+#             */
/*   Updated: 2017/03/03 15:31:03 by ksarnyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static char	*read_one_line(char **map)
{
	static int	n = -1;
	char		*line;

	n++;
	line = *(map + n);
	if (line == NULL)
		n = -1;
	return (line);
}

static int	find_room_with_ant(t_room *rooms, int size, int ant_num)
{
	int i;

	i = 0;
	while (i < size)
	{
		if (rooms[i].current_ant == ant_num)
			return (i);
		i++;
	}
	return (start_room(rooms, size));
}

static void	read_array(t_param *a, char **array, int *ant_num)
{
	int		i;
	int		j;
	char	*room_name;

	i = 0;
	while (array[i])
	{
		j = 1;
		*ant_num = (int)ft_atoi(&array[i][j]);
		while (ft_isdigit(array[i][j]))
			j++;
		room_name = &array[i][++j];
		a->rms[find_room_with_ant(a->rms, a->room_num,\
				*ant_num)].current_ant = 0;
		a->rms[find_index(a->rms, a->room_num, room_name)].\
			current_ant = *ant_num;
		if (a->rms[find_index(a->rms, a->room_num, room_name)].end)
			a->rms[end_room(a->rms, a->room_num)].current_ant_num++;
		i++;
	}
}

static void	reset_rooms(t_param *a, int *ant_num)
{
	a->rms[start_room(a->rms, a->room_num)].current_ant_num =
		a->rms[end_room(a->rms, a->room_num)].current_ant_num;
	a->rms[end_room(a->rms, a->room_num)].current_ant_num = 0;
	draw_lines(a);
	draw_rooms(a);
	*ant_num = 0;
}

void		modify_output(t_param *a)
{
	char		*line;
	char		**array;
	static int	ant_num;

	mlx_clear_window(a->mlx, a->win);
	line = read_one_line(a->map);
	if (line)
	{
		array = ft_strsplit(line, ' ');
		if (a->rms[start_room(a->rms, a->room_num)].current_ant_num != 0)
			a->rms[start_room(a->rms, a->room_num)].\
				current_ant_num += ant_num;
		read_array(a, array, &ant_num);
		if (a->rms[start_room(a->rms, a->room_num)].current_ant_num != 0)
			a->rms[start_room(a->rms, a->room_num)].\
				current_ant_num -= ant_num;
		free_array(array);
		draw_lines(a);
		draw_rooms(a);
	}
	else
		reset_rooms(a, &ant_num);
}
