/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find_solution.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksarnyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/23 14:37:29 by ksarnyts          #+#    #+#             */
/*   Updated: 2017/03/01 15:59:47 by ksarnyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	ft_lstrem(t_list **alst)
{
	t_list *tmp;

	if (*alst)
	{
		tmp = (*alst)->next;
		free((*alst)->content);
		free(*alst);
		*alst = tmp;
	}
}

t_list	*ft_lstsave(t_list *lst)
{
	t_list *new;

	new = NULL;
	while (lst)
	{
		ft_lstadd(&new, ft_lstnew(lst->content, lst->content_size));
		lst = lst->next;
	}
	return (new);
}

int		start_room(t_room *rooms, int size)
{
	int i;

	i = 0;
	while (i < size)
	{
		if (rooms[i].start == 1)
			return (i);
		i++;
	}
	return (-1);
}

int		end_room(t_room *rooms, int size)
{
	int i;

	i = 0;
	while (i < size)
	{
		if (rooms[i].end == 1)
			return (i);
		i++;
	}
	return (-1);
}

int		find_way(t_room *rooms, int size, t_path **path)
{
	int start;
	int end;

	g_steps = 0;
	g_min = size;
	start = start_room(rooms, size);
	end = end_room(rooms, size);
	if (start == -1 || end == -1)
		return (0);
	*path = move_ants(rooms, size, start, end);
	if ((*path)->p1 == NULL)
		return (0);
	*path = move_ants(rooms, size, start, end);
	return (1);
}
