/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_input.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksarnyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/03 14:20:43 by ksarnyts          #+#    #+#             */
/*   Updated: 2017/03/03 15:34:10 by ksarnyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static char	*ft_map_content(int fd)
{
	char *line;
	char *content;

	get_next_line(fd, &line);
	if (!ft_isdigit(line[0]))
	{
		free(line);
		return (NULL);
	}
	content = ft_strnew(1);
	content = ft_join_free_both(content, line);
	content = ft_join_free_first(content, "\n");
	while (get_next_line(fd, &line))
	{
		content = ft_join_free_first(content, line);
		content = ft_join_free_first(content, "\n");
		free(line);
	}
	free(line);
	return (content);
}

static char	**cut_map(char **map)
{
	int		size;
	int		i;
	int		j;
	char	**new_map;

	size = 0;
	i = 0;
	j = 0;
	while (map[i])
	{
		if (map[i][0] == 'L')
			size++;
		i++;
	}
	new_map = (char **)malloc((size + 1) * sizeof(char *));
	if (new_map == NULL)
		return (NULL);
	i = 0;
	while (map[i][0] != 'L')
		i++;
	while (map[i])
		new_map[j++] = ft_strdup(map[i++]);
	new_map[j] = 0;
	free_array(map);
	return (new_map);
}

int			main(int argc, char **argv)
{
	char	*map_content;
	char	**map;
	t_room	*rooms;
	int		room_num;

	if (argc != 1 || !argv[0][0])
		return (ft_printf("usage: ./lem_in < filename | ./visual\n"));
	if ((map_content = ft_map_content(0)) == NULL)
		return (ft_printf("error\n"));
	map = ft_strsplit(map_content, '\n');
	room_num = count_rooms(map);
	rooms = (t_room *)malloc(sizeof(t_room) * room_num);
	create_rooms(map, rooms, 0, 0);
	create_links(map, rooms, room_num);
	ft_printf("%s", map_content);
	map = cut_map(map);
	draw_path(rooms, room_num, map);
	return (0);
}
