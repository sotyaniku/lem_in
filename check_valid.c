/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_valid.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksarnyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/20 13:03:29 by ksarnyts          #+#    #+#             */
/*   Updated: 2017/03/01 15:58:32 by ksarnyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static int		read_coord(char *line)
{
	int i;

	i = 0;
	if (!ft_isdigit(line[i]) && line[i] != '-')
		return (0);
	while (line[++i])
	{
		if (!ft_isdigit(line[i]))
			return (0);
	}
	return (1);
}

int				check_line(char *line)
{
	char	**array;
	int		i;

	array = ft_strsplit(line, ' ');
	i = 0;
	while (array[i])
		i++;
	if (i != 3)
		return (free_array(array));
	if (array[0][0] == '#' || array[0][0] == 'L')
		return (free_array(array));
	i = 0;
	while (++i < 2)
	{
		if (!read_coord(array[i]))
			return (free_array(array));
	}
	free_array(array);
	return (1);
}

static long int	read_num(char *line)
{
	long int	num;
	int			i;

	i = 0;
	while (line[i])
	{
		if (!ft_isdigit(line[i]))
		{
			free(line);
			return (0);
		}
		i++;
	}
	num = ft_atoi(line);
	if (num > 2147483647)
	{
		free(line);
		return (0);
	}
	return (num);
}

static int		check_link(char *line, char *content)
{
	char	**array;
	int		i;
	char	*ptr;
	char	*join;

	array = ft_strsplit(line, '-');
	i = -1;
	while (array[++i])
	{
		join = ft_strjoin("\n", array[i]);
		join = ft_strjoin(join, " ");
		(ptr = ft_strstr(content, join));
		free(join);
		if (ptr == NULL)
			return (free_array(array));
	}
	if (i != 2)
		return (free_array(array));
	free_array(array);
	return (1);
}

char			*check_valid(int fd)
{
	char *line;
	char *content;

	get_next_line(fd, &line);
	if (read_num(line) == 0)
		return (NULL);
	content = ft_strnew(1);
	content = ft_join_free_both(content, line);
	content = ft_join_free_first(content, "\n");
	while (get_next_line(fd, &line))
	{
		if (!(line[0] == '#' && line[1] != '#') &&\
				!(line[0] == '#' && line[1] == '#' && line[2] != '#') &&\
				!check_line(line) && !check_link(line, content))
		{
			free(line);
			free(content);
			return (NULL);
		}
		content = ft_join_free_first(content, line);
		content = ft_join_free_first(content, "\n");
		free(line);
	}
	free(line);
	return (content);
}
