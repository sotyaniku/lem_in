/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   links.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksarnyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/20 16:58:14 by ksarnyts          #+#    #+#             */
/*   Updated: 2017/03/01 16:05:22 by ksarnyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int			find_index(t_room *rooms, int size, char *from)
{
	int i;

	i = 0;
	if (from == NULL)
		return (-1);
	while (i < size)
	{
		if (ft_strcmp(rooms[i].name, from) == 0)
			return (i);
		i++;
	}
	return (-1);
}

static int	basic_check(t_room *rooms, int size)
{
	int i;

	i = 0;
	while (i < size)
	{
		if (rooms[i].start && rooms[i].linked_rooms[0] == NULL)
			return (0);
		if (rooms[i].end && rooms[i].linked_rooms[0] == NULL)
			return (0);
		i++;
	}
	return (1);
}

static void	create_link(char **map, t_room *rooms, int size)
{
	char	**array;
	int		i;
	int		j;

	array = ft_strsplit(*map, '-');
	i = 0;
	while (i < size)
	{
		j = 0;
		if (ft_strcmp(array[0], rooms[i].name) == 0)
		{
			while (rooms[i].linked_rooms[j] != NULL)
				j++;
			rooms[i].linked_rooms[j] = ft_strdup(array[1]);
		}
		else if (ft_strcmp(array[1], rooms[i].name) == 0)
		{
			while (rooms[i].linked_rooms[j] != NULL)
				j++;
			rooms[i].linked_rooms[j] = ft_strdup(array[0]);
		}
		i++;
	}
	free_array(array);
}

int			create_links(char **map, t_room *rooms, int size)
{
	int i;
	int j;

	i = 0;
	while (i < size)
	{
		j = 0;
		rooms[i].linked_rooms =\
					(char **)malloc(sizeof(char *) * size);
		while (j < size)
			(rooms[i].linked_rooms)[j++] = NULL;
		i++;
	}
	while (*map)
	{
		if ((*map)[0] != '#' && !check_line(*map)\
				&& ft_strstr(*map, "-") != NULL)
			create_link(map, rooms, size);
		map++;
	}
	if (!basic_check(rooms, size))
		return (0);
	return (1);
}
