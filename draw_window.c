/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_window.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksarnyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/03 15:38:35 by ksarnyts          #+#    #+#             */
/*   Updated: 2017/03/05 13:45:50 by ksarnyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int		key_function(int key, t_param *a)
{
	if (key == 53)
		exit(0);
	if (key == 124)
		modify_output(a);
	return (0);
}

int		assign_width(t_data *d, t_param *a)
{
	int step;

	step = (1500 / d->width < 1000 / d->height) ? 1500 / d->width :\
		1000 / d->height;
	a->top = 150;
	a->width = step * d->width + a->top * 2;
	a->height = step * d->height + a->top * 2;
	return (step);
}

void	find_width(t_room *rooms, int size, t_param *a)
{
	int		i;
	t_data	d;
	int		step;

	i = 0;
	d.min_x = rooms[0].x;
	d.max_x = rooms[0].x;
	d.min_y = rooms[0].y;
	d.max_y = rooms[0].y;
	while (++i < size)
	{
		d.max_x = MAX(d.max_x, rooms[i].x);
		d.max_y = MAX(d.max_y, rooms[i].y);
		d.min_x = MIN(d.min_x, rooms[i].x);
		d.min_y = MIN(d.min_y, rooms[i].y);
	}
	d.width = (d.max_x - d.min_x > 0) ? d.max_x - d.min_x : 1;
	d.height = (d.max_y - d.min_y > 0) ? d.max_y - d.min_y : 1;
	step = assign_width(&d, a);
	i = -1;
	while (++i < size)
	{
		rooms[i].x = step * (rooms[i].x - d.min_x) + a->top;
		rooms[i].y = step * (rooms[i].y - d.min_y) + a->top;
	}
}

void	draw_path(t_room *rooms, int room_num, char **map)
{
	t_param *a;

	a = (t_param *)malloc(sizeof(*a));
	a->rms = rooms;
	a->room_num = room_num;
	a->map = map;
	find_width(rooms, room_num, a);
	a->mlx = mlx_init();
	a->win = mlx_new_window(a->mlx, a->width, a->height, "lem_in");
	draw_rooms(a);
	mlx_clear_window(a->mlx, a->win);
	draw_lines(a);
	draw_rooms(a);
	mlx_key_hook(a->win, &key_function, a);
	mlx_loop(a->mlx);
}
