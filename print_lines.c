/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_lines.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksarnyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/23 17:18:06 by ksarnyts          #+#    #+#             */
/*   Updated: 2017/03/03 15:33:06 by ksarnyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static int		gradient_x(t_point p1, t_point p2, int grad_x, int x)
{
	int rr;
	int gg;
	int bb;
	int color;

	if (p2.color > p1.color)
		rr = p1.color / (256 * 256) + (p2.color / (256 * 256) -\
				p1.color / (256 * 256)) * x / grad_x;
	else
		rr = p1.color / (256 * 256) - (p1.color / (256 * 256) -\
				p2.color / (256 * 256)) * x / grad_x;
	if (p2.color / 256 % 256 > p1.color / 256 % 256)
		gg = (p1.color / 256) % 256 + ((p2.color / 256) % 256 -\
				(p1.color / 256) % 256) * x / grad_x;
	else
		gg = (p1.color / 256) % 256 - ((p1.color / 256) % 256 -\
				(p2.color / 256) % 256) * x / grad_x;
	if (p2.color % 256 > p1.color % 256)
		bb = p1.color % 256 + (p2.color % 256 -\
				p1.color % 256) * x / grad_x;
	else
		bb = p1.color % 256 - (p1.color % 256 -\
				p2.color % 256) * x / grad_x;
	color = rr * 256 * 256 + gg * 256 + bb;
	return (color);
}

static int		gradient_y(t_point p1, t_point p2, int grad_y, int x)
{
	int rr;
	int gg;
	int bb;
	int color;

	if (p2.color > p1.color)
		rr = p1.color / (256 * 256) + (p2.color /\
				(256 * 256) - p1.color / (256 * 256)) * x / grad_y;
	else
		rr = p1.color / (256 * 256) - (p1.color / (256 * 256)\
				- p2.color / (256 * 256)) * x / grad_y;
	if (p2.color / 256 % 256 > p1.color / 256 % 256)
		gg = (p1.color / 256) % 256 + ((p2.color / 256)\
				% 256 - (p1.color / 256) % 256) * x / grad_y;
	else
		gg = (p1.color / 256) % 256 - ((p1.color / 256) % 256 -\
				(p2.color / 256) % 256) * x / grad_y;
	if (p2.color % 256 > p1.color % 256)
		bb = p1.color % 256 + (p2.color % 256 -\
				p1.color % 256) * x / grad_y;
	else
		bb = p1.color % 256 - (p1.color % 256 -\
				p2.color % 256) * x / grad_y;
	color = rr * 256 * 256 + gg * 256 + bb;
	return (color);
}

static void		print_x(t_point p1, t_point p2, int grad_x, t_param *a)
{
	int x;
	int y;
	int count;

	count = 0;
	if (p1.x < p2.x)
	{
		x = p1.x + 1;
		while (x < p2.x)
		{
			y = p1.y + (x - p1.x) * (p2.y - p1.y) / (p2.x - p1.x);
			mlx_pixel_put(a->mlx, a->win, (x++), y, \
					gradient_x(p1, p2, grad_x, count++));
		}
	}
	else if (p1.x > p2.x)
	{
		x = p1.x - 1;
		while (x > p2.x)
		{
			y = p2.y + (x - p2.x) * (p1.y - p2.y) / (p1.x - p2.x);
			mlx_pixel_put(a->mlx, a->win, (x--), y, \
					gradient_x(p1, p2, grad_x, count++));
		}
	}
}

static void		print_y(t_point p1, t_point p2, int grad_y, t_param *a)
{
	int x;
	int y;
	int k;

	k = 0;
	if (p2.y > p1.y)
	{
		y = p1.y + 1;
		while (y < p2.y)
		{
			x = p1.x + (y - p1.y) * (p2.x - p1.x) / (p2.y - p1.y);
			mlx_pixel_put(a->mlx, a->win, x, (y++), \
					gradient_y(p1, p2, grad_y, k++));
		}
	}
	else if (p2.y < p1.y)
	{
		y = p1.y - 1;
		while (y > p2.y)
		{
			x = p2.x + (y - p2.y) * (p1.x - p2.x) / (p1.y - p2.y);
			mlx_pixel_put(a->mlx, a->win, x, (y--),  \
				gradient_y(p1, p2, grad_y, k++));
		}
	}
}

void			print_lines(t_point p1, t_point p2, t_param *a)
{
	int		grad_x;
	int		grad_y;

	if (p2.x > p1.x)
		grad_x = p2.x - p1.x;
	else
		grad_x = p1.x - p2.x;
	if (p2.y > p1.y)
		grad_y = p2.y - p1.y;
	else
		grad_y = p1.y - p2.y;
	print_x(p1, p2, grad_x, a);
	print_y(p1, p2, grad_y, a);
}
