/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_ants.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksarnyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/03 14:41:23 by ksarnyts          #+#    #+#             */
/*   Updated: 2017/03/03 15:25:03 by ksarnyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	draw_ants(t_param *a, t_room room, int x, int y)
{
	char	*ants;
	int		n;

	if (room.start || room.end)
	{
		ants = ft_itoa(room.current_ant_num);
		n = (int)ft_strlen(ants);
		mlx_string_put(a->mlx, a->win, x - 20 - 6 * n, y - 30,\
		0xFFFFFF, ants);
		if (room.start)
			mlx_string_put(a->mlx, a->win, x - 40, y - 65, 0xFFFFFF,\
			"start");
		else if (room.end)
			mlx_string_put(a->mlx, a->win, x - 40, y - 65, 0xFFFFFF,\
			"end");
	}
	else
	{
		ants = ft_itoa(room.current_ant);
		n = (int)ft_strlen(ants);
		mlx_string_put(a->mlx, a->win, x - 20 - 6 * n, y - 30,\
		0x000000, ants);
	}
}

int		change_color(int color)
{
	int bb;
	int rr;
	int gg;

	rr = color >> 16;
	gg = (color >> 8) & 0x0000FF;
	bb = color & 0x0000FF;
	rr = (rr + 50) % 256;
	gg = (gg + 30) % 256;
	bb = (bb + 70) % 256;
	color = (rr << 16) | (gg << 8) | (bb);
	return (color);
}

void	draw_lines(t_param *a)
{
	int		i;
	int		j;
	int		index;
	t_point	p1;
	t_point	p2;

	i = 0;
	while (i < a->room_num)
	{
		j = 0;
		p1.x = a->rms[i].x;
		p1.y = a->rms[i].y;
		p1.color = a->rms[i].color;
		while (a->rms[i].linked_rooms[j])
		{
			index = find_index(a->rms, a->room_num, \
				a->rms[i].linked_rooms[j]);
			p2.x = a->rms[index].x;
			p2.y = a->rms[index].y;
			p2.color = a->rms[index].color;
			print_lines(p1, p2, a);
			j++;
		}
		i++;
	}
}

void	set_color(t_room *room, int color)
{
	if (room->start)
		room->color = 0xae0000;
	else if (room->end)
		room->color = 0x0000ae;
	else
		room->color = color;
}

void	draw_rooms(t_param *a)
{
	int i;
	int x;
	int y;
	int color;

	i = 0;
	color = 0xa0a0a0;
	while (i < a->room_num)
	{
		set_color(&(a->rms[i]), color);
		y = a->rms[i].y - 20;
		while (y < a->rms[i].y + 20)
		{
			x = a->rms[i].x - 20;
			while (x < a->rms[i].x + 20)
				mlx_pixel_put(a->mlx, a->win, (x++), y,\
				a->rms[i].color);
			y++;
		}
		draw_ants(a, a->rms[i], x, y);
		mlx_string_put(a->mlx, a->win, x - 40, y + 5,\
		a->rms[i].color, a->rms[i].name);
		color = change_color(color);
		i++;
	}
}
