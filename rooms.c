/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rooms.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksarnyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/03 15:34:41 by ksarnyts          #+#    #+#             */
/*   Updated: 2017/03/03 15:34:43 by ksarnyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int			count_rooms(char **map)
{
	int i;
	int count;

	i = 0;
	count = 0;
	while (map[i])
	{
		if (map[i][0] != '#' && check_line(map[i]))
			count++;
		i++;
	}
	return (count);
}

static int	create_room(char *line, t_room *rooms, char *flag, int ants)
{
	char **room;

	rooms->current_ant_num = 0;
	rooms->current_ant = 0;
	rooms->visited = 0;
	room = ft_strsplit(line, ' ');
	rooms->name = ft_strdup(room[0]);
	rooms->x = (int)ft_atoi(room[1]);
	rooms->y = (int)ft_atoi(room[2]);
	rooms->linked_rooms = NULL;
	if (ft_strcmp(flag, "start") == 0)
	{
		rooms->start = 1;
		rooms->end = 0;
		rooms->current_ant_num = ants;
	}
	else
	{
		if (ft_strcmp(flag, "end") == 0)
			rooms->end = 1;
		else
			rooms->end = 0;
		rooms->start = 0;
	}
	return (!free_array(room));
}

static int	check_flag(char *command, int *start, int *end)
{
	if (command[0] == '#')
	{
		if (!ft_strcmp(command, "##start"))
		{
			if (*start == 0)
				*start = 1;
			else
				return (0);
		}
		else if (!ft_strcmp(command, "##end"))
		{
			if (*end == 0)
				*end = 1;
			else
				return (0);
		}
	}
	return (1);
}

static void	assign_flags(int *start, int *end)
{
	if (*start == 1)
		*start = 2;
	if (*end == 1)
		*end = 2;
}

int			create_rooms(char **map, t_room *rooms, int start, int end)
{
	int ant_num;
	int i;
	int count;

	i = 0;
	count = 0;
	ant_num = (int)ft_atoi(map[0]);
	while (map[++i])
	{
		if (!check_flag(map[i], &start, &end))
			return (0);
		else if (check_line(map[i]))
		{
			if (start == 1)
				create_room(map[i], &rooms[count++], "start", ant_num);
			else if (end == 1)
				create_room(map[i], &rooms[count++], "end", ant_num);
			else
				create_room(map[i], &rooms[count++], "0", ant_num);
			assign_flags(&start, &end);
		}
	}
	if (start != 2 || end != 2)
		return (0);
	return (1);
}
