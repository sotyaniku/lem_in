/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksarnyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/27 17:02:32 by ksarnyts          #+#    #+#             */
/*   Updated: 2017/03/01 16:00:56 by ksarnyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int			free_array(char **array)
{
	int i;

	i = 0;
	while (array[i])
	{
		free(array[i]);
		i++;
	}
	free(array);
	return (0);
}

void		ft_lst_cont_clean(void *content, size_t size)
{
	ft_bzero(content, size);
	free(content);
}

static void	free_rooms(t_room *rooms, int size)
{
	int i;

	i = 0;
	while (i < size)
	{
		free(rooms[i].name);
		free_array(rooms[i].linked_rooms);
		i++;
	}
	free(rooms);
}

void		free_all(t_room *rooms, int size, t_path *path, char *map_content)
{
	free_rooms(rooms, size);
	ft_lstdel(&(path->p1), &ft_lst_cont_clean);
	ft_lstdel(&(path->p2), &ft_lst_cont_clean);
	ft_lstdel(&(path->rev1), &ft_lst_cont_clean);
	ft_lstdel(&(path->rev2), &ft_lst_cont_clean);
	free(path);
	free(map_content);
}
