/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   helpers.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksarnyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/04 17:47:25 by ksarnyts          #+#    #+#             */
/*   Updated: 2017/03/05 15:41:07 by ksarnyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int		move_ant(t_room *rooms, int size, char *from, char *to)
{
	int i;
	int j;

	if ((i = find_index(rooms, size, from)) == -1 ||\
		(j = find_index(rooms, size, to)) == -1)
		return (0);
	if (rooms[j].visited)
		return (0);
	rooms[i].visited = 1;
	g_steps++;
	return (1);
}

int		move_back(t_room *rooms, int size, char *to, char *from)
{
	int i;

	if ((i = find_index(rooms, size, from)) == -1 ||\
		(find_index(rooms, size, to)) == -1)
		return (0);
	rooms[i].visited = 0;
	g_steps--;
	return (1);
}

int		move_ant_count(t_room *rooms, int size, char *from, char *to)
{
	int			i;
	int			j;
	static int	num = 1;

	if ((i = find_index(rooms, size, from)) == -1 ||\
		(j = find_index(rooms, size, to)) == -1)
		return (0);
	if (rooms[i].current_ant_num == 0)
		return (0);
	if (rooms[j].current_ant_num && rooms[j].end == 0)
		return (0);
	if (rooms[i].start)
	{
		rooms[i].current_ant = num;
		num++;
	}
	rooms[i].current_ant_num--;
	rooms[j].current_ant_num++;
	rooms[j].current_ant = rooms[i].current_ant;
	return (rooms[j].current_ant);
}

void	move_all_to_end(t_room *rooms, int size)
{
	int i;
	int end;
	int start;

	i = 0;
	start = rooms[start_room(rooms, size)].current_ant_num;
	end = end_room(rooms, size);
	while (++i <= start)
		ft_printf("L%d-%s ", i, rooms[end].name);
	ft_printf("\n");
}
