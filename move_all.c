/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_all.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ksarnyts <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/27 17:03:51 by ksarnyts          #+#    #+#             */
/*   Updated: 2017/03/05 15:42:10 by ksarnyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

t_list		*reverse_path(t_room *rooms, int size, t_list *path)
{
	t_list *tmp;

	tmp = NULL;
	while (path)
	{
		ft_lstadd(&tmp, ft_lstnew(path->content, path->content_size));
		path = path->next;
	}
	ft_lstaddlast(&tmp, ft_lstnew(rooms[start_room(rooms, size)].name,\
		ft_strlen(rooms[start_room(rooms, size)].name) + 1));
	return (tmp);
}

static void	move_twice(t_room *rooms, int size, t_path *path, int n)
{
	int flag;
	int ant_num;

	flag = 0;
	while (n-- > 0)
	{
		path->tmp2 = path->tmp2->next;
		if (path->p1)
			path->tmp1 = path->tmp1->next;
	}
	while (path->tmp2->next)
	{
		if ((ant_num = move_ant_count(rooms, size,\
			path->tmp2->next->content, path->tmp2->content)))
			ft_printf("L%d-%s ", ant_num, path->tmp2->content);
		if (path->tmp1 && path->tmp1->next && (ant_num = move_ant_count(rooms,\
					size, path->tmp1->next->content, path->tmp1->content)))
			ft_printf("L%d-%s ", ant_num, path->tmp1->content);
		path->tmp2 = path->tmp2->next;
		if (path->tmp1)
			path->tmp1 = path->tmp1->next;
		flag = 1;
	}
	(flag) ? ft_printf("\n") : ft_printf("\0");
}

static void	move_once(t_room *rooms, int size, t_path *path, int n)
{
	int	ant_num;
	int	flag;

	path->tmp1 = path->rev1;
	path->tmp2 = path->rev2;
	flag = 0;
	if (path->p2)
		return (move_twice(rooms, size, path, n));
	while (n-- > 0)
		path->tmp1 = path->tmp1->next;
	while (path->tmp1->next)
	{
		if ((ant_num = move_ant_count(rooms, size,\
		path->tmp1->next->content, path->tmp1->content)))
			ft_printf("L%d-%s ", ant_num, path->tmp1->content);
		path->tmp1 = path->tmp1->next;
		flag = 1;
	}
	(flag) ? ft_printf("\n") : ft_printf("\0");
}

static void	move_all_ants(t_room *rooms, int size, t_path *path)
{
	int		ants;
	int		end_index;
	int		counter;
	t_list	*ptr1;
	t_list	*ptr2;

	ants = rooms[start_room(rooms, size)].current_ant_num;
	ptr1 = path->p1;
	ptr2 = path->p2;
	path->rev1 = reverse_path(rooms, size, path->p1);
	path->rev2 = reverse_path(rooms, size, path->p2);
	end_index = end_room(rooms, size);
	counter = ft_lstsize(path->p1) - 1;
	if (!ft_strcmp(path->p1->content, rooms[end_room(rooms, size)].name))
		return (move_all_to_end(rooms, size));
	while (ptr1->next)
	{
		move_once(rooms, size, path, counter);
		ptr1 = ptr1->next;
		if (ptr2)
			ptr2 = ptr2->next;
		counter--;
	}
	while (rooms[end_index].current_ant_num != ants)
		move_once(rooms, size, path, counter);
}

int			main(int argc, char **argv)
{
	char	*map_content;
	char	**map;
	t_room	*rooms;
	int		room_num;
	t_path	*path;

	if (argc != 1 || !argv[0][0])
		return (ft_printf("usage: ./lem_in < filename\n"));
	if ((map_content = check_valid(0)) == NULL)
		return (ft_printf("not valid file\n"));
	map = ft_strsplit(map_content, '\n');
	room_num = count_rooms(map);
	rooms = (t_room *)malloc(sizeof(t_room) * room_num);
	if (!create_rooms(map, rooms, 0, 0))
		return (ft_printf("rooms: not valid file\n"));
	if (!create_links(map, rooms, room_num))
		return (ft_printf("links between rooms: not valid file\n"));
	path = NULL;
	if (!find_way(rooms, room_num, &path))
		return (ft_printf("no possible solution found\n"));
	ft_printf("%s\n", map_content);
	move_all_ants(rooms, room_num, path);
	free_array(map);
	free_all(rooms, room_num, path, map_content);
	return (0);
}
