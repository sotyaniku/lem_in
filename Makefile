# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: ksarnyts <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/02/16 10:42:36 by ksarnyts          #+#    #+#              #
#    Updated: 2017/03/03 18:01:50 by ksarnyts         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = lem_in

LIBFT = libft.a

LIBFTPRINTF = libftprintf.a

CFLAGS = -Wall -Wextra -Werror

SRC =	move_all.c \
       check_valid.c\
       rooms.c \
       links.c \
       find_solution.c\
       move_one_ant.c \
       helpers.c \
       free.c

SRC2 = check_valid.c\
       rooms.c \
       links.c \
       find_solution.c\
       helpers.c\
       move_one_ant.c \
       free.c \
       print_lines.c\
       read_input.c\
       draw_ants.c\
       draw_window.c\
       modify.c

FRAME = -lmlx -framework OpenGL -framework AppKit

OBJ = $(SRC:.c=.o)

OBJ2 = $(SRC2:.c=.o)

%.o:%.c
	gcc $(CFLAGS) -c -o $@ $<

all: $(NAME)

$(LIBFT):
	cd libft/ && $(MAKE)

$(LIBFTPRINTF):
	cd libft/printf/ && $(MAKE)

$(NAME): $(OBJ) $(OBJ2) $(LIBFT) $(LIBFTPRINTF)
	gcc -o $(NAME) $(OBJ) libft/$(LIBFT) libft/printf/$(LIBFTPRINTF)
	gcc $(FRAME) -o visu-hex $(OBJ2) libft/libft.a \
	libft/printf/libftprintf.a

clean:
	rm -f $(OBJ)
	rm -f $(OBJ2)
	cd libft/ && $(MAKE) clean
	cd libft/printf/ && $(MAKE) clean

fclean: clean
	rm -f $(NAME)
	rm -f visu-hex
	cd libft/ && rm -f libft.a
	cd libft/printf/ && rm -f libftprintf.a

re: fclean all
